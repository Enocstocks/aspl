$: << File.expand_path("lib")

require "aspl/version"

Gem::Specification.new do |s|
  s.name        = "aspr"
  s.version     = ASPL::VERSION
  s.summary     = "a REPL for x86-64 Assembly"
  s.description = "Write x86-64 Assembly in a REPL"
  s.authors     = ["Bryan Hernández"]
  s.email       = "masterenoc@tutamail.com"
  s.files       = `git ls-files -z`.split("\x0")
  s.test_files  = s.files.grep(%r{^test/})
  s.homepage    = "https://codeberg.org/Enocstocks/aspl"
  s.license     = "AGPL-3.0-or-later"
  s.bindir      = "bin"

  s.executables << "aspl"

  s.add_development_dependency 'minitest', '~> 5.14'
  s.add_development_dependency 'crabstone', '~> 4.0'
  s.add_development_dependency 'rake', '~> 13.0'
  s.add_development_dependency 'debug', '~> 1.0.0'
  s.add_dependency 'fisk', '~> 2.3.1'
end
