# Copyright 2021 Aaron Patterson

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module ASPL
  class Assembler
    def assemble(ast)
      fisk = Fisk.new

      case ast
      in [:command, [:instruction, insn], [:register, r], [:int, n]]
        possibles = insn.forms.find_all do |form|
          form.operands.first.type == r.type
        end
        l = if possibles.any? { |form| form.operands[1].type == n.to_s }
          fisk.lit(n)
        else
          fisk.imm(n)
        end
        fisk.gen_with_insn insn, [r, l]
      in [:command, [:instruction, insn], [:register, r], [:register, r2]]
        fisk.gen_with_insn insn, [r, r2]
      in [:command, [:instruction, insn], [:register, r], [:memory, mem]]
        fisk.gen_with_insn insn, [r, mem]
      in [:command, [:instruction, insn], [:memory, a], [:register, b]]
        fisk.gen_with_insn insn, [a, b]
      in [:command, [:instruction, insn], [:int, n]]
        forms = insn.forms

        l = if forms.any? { |form| form.operands[0].type == n.to_s }
          fisk.lit(n)
        else
          fisk.imm(n)
        end
        fisk.gen_with_insn insn, [l]

      in [:command, [:instruction, insn], [:register, n]]
        fisk.gen_with_insn insn, [n]
      in [:command, [:instruction, insn], [:memory, n]]
        fisk.gen_with_insn insn, [n]
      in [:command, [:instruction, insn], [:memory, n], [:int, b]]
        fisk.gen_with_insn insn, [n, fisk.imm(b)]
      in [:command, [:instruction, insn]]
        fisk.gen_with_insn insn, []
      else
        p ast
        raise "Unknown"
      end

      fisk.to_binary
    end
  end
end
