# This file is part of aspl.

# Foobar is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# Foobar is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# Foobar. If not, see <https://www.gnu.org/licenses/>.

require "fisk/helpers"
require "aspl/thread_state"

module ASPL
  module Linux
    include Fiddle

    def self.make_function name, args, ret
      ptr = Handle::DEFAULT[name]
      func = Function.new ptr, args, ret, name: name
      define_singleton_method name, &func.to_proc
    end

    # from bits/mman-linux.h
    MAP_SHARED = 0x01

    make_function "ptrace", [TYPE_INT, TYPE_INT, TYPE_VOIDP, TYPE_VOIDP], TYPE_INT
    make_function "memset", [TYPE_VOIDP, TYPE_INT, TYPE_SIZE_T], TYPE_VOID

    def self.mmap_jit size
      ptr = Fisk::Helpers.mmap 0, size, Fisk::Helpers::PROT_READ |
                                        Fisk::Helpers::PROT_WRITE |
                                        Fisk::Helpers::PROT_EXEC,
                               MAP_SHARED | Fisk::Helpers::MAP_ANON, -1, 0

      ptr.size = size
      ptr
    end

    def self.jitbuffer size
      Fisk::Helpers::JITBuffer.new mmap_jit(size), size
    end

    # from sys/ptrace.h
    PTRACE_TRACEME = 0
    PTRACE_CONT = 7
    PTRACE_GETREGS = 12
    PTRACE_SETREGS = 13

    def self.traceme
      raise unless ptrace(PTRACE_TRACEME, 0, 0, 0).zero?
    end

    fields = (<<-eostruct).scan(/int ([^;]*);/).flatten
struct user_regs_struct
{
  __extension__ unsigned long long int r15;
  __extension__ unsigned long long int r14;
  __extension__ unsigned long long int r13;
  __extension__ unsigned long long int r12;
  __extension__ unsigned long long int rbp;
  __extension__ unsigned long long int rbx;
  __extension__ unsigned long long int r11;
  __extension__ unsigned long long int r10;
  __extension__ unsigned long long int r9;
  __extension__ unsigned long long int r8;
  __extension__ unsigned long long int rax;
  __extension__ unsigned long long int rcx;
  __extension__ unsigned long long int rdx;
  __extension__ unsigned long long int rsi;
  __extension__ unsigned long long int rdi;
  __extension__ unsigned long long int orig_rax;
  __extension__ unsigned long long int rip;
  __extension__ unsigned long long int cs;
  __extension__ unsigned long long int eflags;
  __extension__ unsigned long long int rsp;
  __extension__ unsigned long long int ss;
  __extension__ unsigned long long int fs_base;
  __extension__ unsigned long long int gs_base;
  __extension__ unsigned long long int ds;
  __extension__ unsigned long long int es;
  __extension__ unsigned long long int fs;
  __extension__ unsigned long long int gs;
};
    eostruct

    class ThreadState < ASPL::ThreadState.build(fields)
      private

      def read_flags;
        eflags
      end

      def other_registers
        super - ["orig_rax"]
      end
    end

    class Tracer
      def initialize pid
        @pid = pid
      end

      def wait
        Process.waitpid @pid
      end

      def state
        state = ThreadState.malloc
        raise unless Linux.ptrace(PTRACE_GETREGS, @pid, 0, state).zero?
        state
      end

      def state= state
        raise unless Linux.ptrace(PTRACE_SETREGS, @pid, 0, state).zero?
        state
      end

      def continue
        unless Linux.ptrace(Linux::PTRACE_CONT, @pid, 1, 0).zero?
          raise
        end
      end

    end

  end
end
