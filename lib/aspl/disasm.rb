# Copyright 2021 Aaron Patterson

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require "crabstone"

class Crabstone::Binding::Instruction
  class << self
    alias :old_release :release
  end

  # Squelch error in crabstone
  def self.release obj
    nil
  end
end

module ASPL
  module Disasm
    def self.disasm buffer
      binary = buffer.memory[0, buffer.pos]
      cs = Crabstone::Disassembler.new(Crabstone::ARCH_X86, Crabstone::MODE_64)
      cs.disasm(binary, buffer.memory.to_i).each {|i|
        puts "%s %s" % [i.mnemonic, i.op_str]
      }
    end
  end
end
