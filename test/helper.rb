require "aspl"
require "minitest"
require "minitest/autorun"
require "crabstone"
require "minitest/pride"

class Crabstone::Binding::Instruction
  class << self
    alias :old_release :release
  end

  # Squelch error in crabstone
  def self.release obj
    nil
  end
end

module ASPL
  class Test < Minitest::Test
  end
end
